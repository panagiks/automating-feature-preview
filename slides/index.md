# whoami

- Kolokotronis Panagiotis. Find me @ [panagiks.com](https://panagiks.com)
- IT Architect @ noris M.I.K.E.
- Python, Security, Open Source
- CITA-F | CVE-2018-1000519 | CVE-2018-1000814

---

# Company

///

## noris M.I.K.E.

- noris network AG subsidiary
- supports parent company in:
  - Development Projects
  - Automation
  - Operations (Win/Lin/Cloud)
  - Security Operations Center (SOC)

///

## noris network AG

- Premium IT Service Provider
- Datacenter-as-a-Service
- Colocation
- Managed IT Services
- Public / Private Cloud solutions
- [Certifications](https://www.noris.de/en/company/certifications/)

---

# Automating Feature Preview
### Continuous Delivery on steroids

///

## Purposes of the workshop

Yes:
- Showcase the synergy between GitLab-CI and OpenShift
- (hopefully) Provide a 'quality of life' feature for Developers and Product Owners

No:
- Provide an exaustive how-to

///

## Prerequisites & Nice-to-haves

- [P] Basic understanding of git
- [P] Basic knowledge of modern development roles & workflows
- [N] Basic CI/CD principles & basic GitLab-CI knowledge
- [N] Understanding of k8s / OpenShift ecosystem

///

## Table of Content

- Definitions
- Tools & Processes used
- GitLab Review Apps
- OpenShift Template objects
- Why not 'just' GitLab Pages
- Example app
- Demo
- References

---

# Definitions

///

## What is CD

_Continuous Deployment_ or _Continuous Delivery_

Automates:

- Keeping the latest working version always available
- Delivering multiple small changes vs Delivering one big block of multiple changes
- Deployment of the application on its various environments

---

# Tools & Processes

- GitLab-CI
- OpenShift (OKD / OCP 4.10)

///

## GitLab-CI

- CI/CD platform by GitLab
- Pipeline as Code => Versioned, auditable, centralized (platform) yet distributed (project)
- SaaS & Self-Hosted

///

## GitLab Environments & Review Apps

- An Environment is an abstract representation of the "where" code is deployed
- Review Apps are a toolset to provide an environment / branch for showcasing changes

///

## OpenShift

Kubernetes on Steroids!

- Redhat product
- Implements the K8s API and extends it with enterprise-focused features
- Open Source flavor is called OKD
- Enterprise flavor is called OCP

---

# GitLab Review Apps

///

## About

- Declarative definition of environment setup & teardown
- Agnostic of the deployment target
- Provide visibility & control into past and present environments
- Annotate Merge Requests with:
  - Control buttons for the branch's environment
  - Link to the deployed Review App

///

## Requirements

- The project uses GitLab-CI
- A stage named `preview` is included in the project's CI
- There is a job in the `preview` stage which defines an `environment` section
- The `environment` section includes at least the following fields:
  - `name` (required to enable Review Apps)
  - `url` (required to embed the "View app" button on Merge Requests)

///

## Minimal `.gitlab-ci.yaml`

```yaml
stages:
- preview

preview_deploy:
  image: alpine
  stage: preview
  script:
  - mkdir dist
  - echo "<!doctype html><html lang="en"><body>Review App for Merge Request ${CI_MERGE_REQUEST_IID}</body></html>" > dist/index.html
  environment:
    name: $CI_PROJECT_NAME/$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
    url: $CI_PROJECT_URL/-/jobs/$CI_JOB_ID/artifacts/file/dist/index.html
  artifacts:
    paths:
    - dist
  only:
  - merge_requests
```

---

# OpenShift Template objects

///

## About

- OpenShift-specific API
- Describes a set of parameterizable objects
- When processed produces a list of objects
- Ideal for describing a self-contained instance of an application

///

## Minimal Template example

```yaml
apiVersion: template.openshift.io/v1
kind: Template
metadata:
  name: my-template
objects:
- kind: Pod
  apiVersion: v1
  metadata:
    name: example
  spec:
    containers:
      - name: nginx
        image: nginxinc/nginx-unprivileged
```

---

# Why not 'just' GitLab Pages?

- Can only host static content
- The URL is predictable but long / ugly
- Security (Session & XSRF-Token Cookie domain)

---

# Example App

A simple demo App found [here](https://gitlab.com/panagiks/automating-feature-preview-demo).

- Simple static html page served by nginx
- Dockerized
- Has OpenShift object definitions
- Has GitLab-CI

///

## A fictive development workflow (1/2)

- A task is selceted for develpment
- A developer is working on the task in a feature branch
- When the task is done the developer creates a merge request
- An integration links the merge request into the task
- Automated tests run and pass
- Task moves into peer-review

///

## A fictive development workflow (2/2)

- Another developer is reviewing the merge request
- Task moves into Product Owner review
- The Product owner visits the merge request and clicks the preview link
- The task is moved to ready
- The merge request is accepted
- Automated build & deploy process releases the feature into production

---

# Demo

![](slides/images/demo_meme.jpg)

---

# Like what you see?
## We're hiring!

- DevOps / LinOps / WinOps
- SRE
- Sr. Security IT Engineer
- Data Engineer
- More @ https://jobs.noris.gr

---

# Questions ?
## Get the slides

![](slides/images/qrcode.png)

---

# References

- [Demo App](https://gitlab.com/panagiks/automating-feature-preview-demo)
- [GitLab Review Apps](https://docs.gitlab.com/ee/ci/review_apps/)
- [OpenShift Templates](https://docs.openshift.com/container-platform/4.10/openshift_images/using-templates.html)
